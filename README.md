# Go IP Change Notifier
[![Pipeline status](https://img.shields.io/gitlab/pipeline/palica/go-ip-change-notifier/master.svg)](https://gitlab.com/palica/go-ip-change-notifier/pipelines?scope=branches) [![goreport badge](https://goreportcard.com/badge/gitlab.com/palica/go-ip-change-notifier)](https://goreportcard.com/report/gitlab.com/palica/go-ip-change-notifier)

The program will check external ip address and if it changed, 
it will send a notification email.

This is a simple project to learn Golang.
