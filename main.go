// This program should be able to:
// - notice a changed external IP address
// - send a notification to a specified e-mail address

package main

import (
	"fmt"
	"log"
	"net/smtp"
	"os"

	ipify "github.com/rdegges/go-ipify"
	"github.com/spf13/viper"
	yaml "gopkg.in/yaml.v2"
)

// IP stores the ip address
var IP string

// EmailUser contains data structures for username, password, server and port
// to be used for sending email.
type EmailUser struct {
	Username    string
	Password    string
	EmailServer string
	Port        int
}

func init() {
	fmt.Println("Testing the init function!")
}

func main() {

	configureApplication()
	IP = getIP()
	if ipHasChanged() {
		//true
		fmt.Println("New IP! " + IP)
		fmt.Println("Last IP " + viper.Get("last_ip").(string))
		viper.Set("last_ip", IP)
		// sendEmail()
		updateConfig()
	} else {
		// false
		fmt.Println("No change!")
	}

	// run forever
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	<-sigChan

}

// configureApplication is using viper to configure various application variables
func configureApplication() {
	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("yaml") // extension and format of the config file
	viper.AddConfigPath("/etc/ipnotify/")  // path to look for the config file in
	viper.AddConfigPath("$HOME/.ipnotify") // call multiple times to add many search paths
	viper.AddConfigPath(".")               // optionally look for config in the working directory
	viper.SetDefault("project", "IP-Notifier")
	viper.SetDefault("version", "0.2") // :)
	err := viper.ReadInConfig()        // Find and read the config file
	if err != nil {                    // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s ", err))
	}

	fmt.Println("Program to check external IP and notify on changes via email.")
	fmt.Printf("Using %s as config file.\n", viper.ConfigFileUsed())
	fmt.Println(viper.Get("project"), viper.Get("version"))

	if viper.Get("last_ip") == nil {
		IP = getIP()
		fmt.Println("No previous IP stored. Storing your IP now.", IP)
		viper.Set("last_ip", IP)
		fmt.Println("These are the settings to be written:")
		fmt.Println(yamlStringSettings())
		fmt.Println("Writing new settings... Done.")
		updateConfig()
	}
}

func yamlStringSettings() string {
	c := viper.AllSettings()
	bs, err := yaml.Marshal(c)
	if err != nil {
		log.Fatalf("unable to marshal config to YAML: %v", err)
	}
	return string(bs)
}

func updateConfig() {
	file, err := os.Create("config.yaml")
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()

	fmt.Fprintf(file, yamlStringSettings())
}

func sendEmail() {

	emailUser := &EmailUser{
		viper.Get("email_username").(string),
		viper.Get("email_password").(string),
		viper.Get("email_server").(string),
		viper.Get("email_port").(int),
	}

	auth := smtp.PlainAuth("",
		emailUser.Username,
		emailUser.Password,
		emailUser.EmailServer,
	)

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	to := []string{"p@e", "h@m"}
	msg := []byte("To: p@e\r\n" +
		"To: h@m\r\n" +
		"Subject: IP Changed!\r\n" +
		"\r\n" +
		"Your IP changed to:.\r\n" +
		IP) // will this work? removing for now
	err := smtp.SendMail("smtp.gmail.com:587", auth, "p.c@m", to, msg)
	if err != nil {
		log.Fatal(err)
	}
}

func ipHasChanged() bool {
	if IP != viper.Get("last_ip") {
		return true
	}
	return false
}

func getIP() string {
	ip, err := ipify.GetIp()
	if err != nil {
		fmt.Println("Couldn't get my IP address:", err)
	}
	return ip
}
